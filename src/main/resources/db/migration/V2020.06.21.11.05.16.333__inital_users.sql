INSERT INTO public.users(username, email, password, role, enabled, full_name, phone, created_at, city_id, version)
    VALUES ('admin', 'admin@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'ADMIN', true, 'admin', '+38977123456', CURRENT_TIMESTAMP + interval '1' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testPlayer1', 'test-player@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'PLAYER', true, 'player', '+38977123457', CURRENT_TIMESTAMP + interval '2' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testOperator1', 'test-operator@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'OPERATOR', true, 'operator', '+38977123458', CURRENT_TIMESTAMP + interval '3' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testPlayer2', 'test-player2@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'PLAYER', true, 'player2', '+38977123459', CURRENT_TIMESTAMP + interval '4' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testPlayer3', 'test-player3@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'PLAYER', true, 'player3', '+38977123411', CURRENT_TIMESTAMP + interval '5' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testPlayer4', 'test-player4@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'PLAYER', true, 'player4', '+38977123412', CURRENT_TIMESTAMP + interval '6' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testPlayer5', 'test-player5@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'PLAYER', true, 'player5', '+38977123413', CURRENT_TIMESTAMP + interval '7' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0),
    ('testPlayer6', 'test-player6@test.mk', '{bcrypt}$2a$10$59wW0rpUY.G3ve9lY9rst.chZ.DsYzzFwhieT3j8.piLYvUYV895i', 'PLAYER', true, 'player6', '+38977123414', CURRENT_TIMESTAMP + interval '8' day,
    (SELECT id from city where name = 'ЦЕНТАР'), 0);


INSERT INTO group_members(username, group_id) VALUES ('admin', 1);
INSERT INTO group_members(username, group_id) VALUES ('testOperator1', 2);
INSERT INTO group_members(username, group_id) VALUES ('testPlayer1', 3);
INSERT INTO group_members(username, group_id) VALUES ('testPlayer2', 3);
INSERT INTO group_members(username, group_id) VALUES ('testPlayer3', 3);
INSERT INTO group_members(username, group_id) VALUES ('testPlayer4', 3);
INSERT INTO group_members(username, group_id) VALUES ('testPlayer5', 3);
INSERT INTO group_members(username, group_id) VALUES ('testPlayer6', 3);