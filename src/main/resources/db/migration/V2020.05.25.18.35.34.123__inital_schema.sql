
CREATE TABLE city (
    id bigint NOT NULL,
    code character varying(4),
    name character varying(50),
    version bigint,
    constraint city_pkey primary key (id)
);

CREATE SEQUENCE IF NOT EXISTS city_seq
    INCREMENT BY 10
    START WITH 1;

--------------------------------------------------------------------------------------------------

CREATE TABLE groups (
    id serial NOT NULL,
    group_name character varying(50) NOT NULL,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    constraint groups_pkey primary key (id)
);

INSERT INTO groups VALUES (1, 'ADMIN_GROUP', NULL, NULL);
INSERT INTO groups VALUES (2, 'OPERATOR_GROUP', NULL, NULL);
INSERT INTO groups VALUES (3, 'PLAYER_GROUP', NULL, NULL);

------------------------------------------------------

CREATE TABLE users (
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL UNIQUE,
    role VARCHAR(255) NOT NULL,
    enabled boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    full_name character varying(255) NOT NULL,
    activation_key character varying(50),
    activation_key_created_at timestamp with time zone,
    reset_key character varying(50),
    reset_key_created_at timestamp with time zone,
    address character varying(255),
    phone character varying(255) NOT NULL UNIQUE,
    age integer,
    is_employed boolean,
    version bigint,
    city_id bigint not null,
    constraint users_pkey primary key (username),
    constraint fk_users_city FOREIGN KEY (city_id)
         REFERENCES city (id)
         ON DELETE NO ACTION
         ON UPDATE NO ACTION
);

-----------------------------------------------------

CREATE TABLE group_authorities (
    group_id serial NOT NULL,
    authority character varying(50) NOT NULL,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    id integer NOT NULL,
    constraint group_authorities_pkey primary key (id),
    CONSTRAINT fk_group_authorities_group
        FOREIGN KEY (group_id)
        REFERENCES groups (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

INSERT INTO group_authorities VALUES (1, 'AUTHORIZED_ADMIN', NULL, NULL, 2);
INSERT INTO group_authorities VALUES (2, 'AUTHORIZED_OPERATOR', NULL, NULL, 3);
INSERT INTO group_authorities VALUES (3, 'AUTHORIZED_PLAYER', NULL, NULL, 1);

--------------------------------------

CREATE TABLE group_members (
    id serial NOT NULL,
    username character varying(255) NOT NULL,
    group_id integer NOT NULL,
    created_at timestamp with time zone,
    modified_at timestamp with time zone,
    constraint group_members_pkey primary key (id),
    CONSTRAINT fk_group_members_users
        FOREIGN KEY (username)
        REFERENCES users (username)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_group_members_group
        FOREIGN KEY (group_id)
        REFERENCES groups (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

----------------------------------------------------------

CREATE TABLE oauth_access_token (
    token_id character varying(256),
    token bytea,
    authentication_id character varying(256) NOT NULL,
    user_name character varying(256),
    client_id character varying(256),
    authentication bytea,
    refresh_token character varying(256),
    constraint oauth_access_token_pkey primary key (authentication_id)
);

-----------------------------------------------------------------------

CREATE TABLE oauth_client_details (
    client_id character varying(256) NOT NULL,
    resource_ids character varying(256),
    client_secret character varying(256) NOT NULL,
    scope character varying(256),
    authorized_grant_types character varying(256),
    web_server_redirect_uri character varying(256),
    authorities character varying(256),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information character varying(4000),
    autoapprove character varying(256),
    constraint oauth_client_details_pkey primary key (client_id)
);

INSERT INTO public.oauth_client_details VALUES ('public-android-client', NULL, '{bcrypt}$2a$10$7JVJT.LxsZRJ48fpcHQrdeYytSA/8IZxgTqt3WS50AMPe.HAcBcmC', 'read,write', 'password,authorization_code,refresh_token', NULL, 'ROLE_CLIENT', 1800, 86400, NULL, NULL);
INSERT INTO public.oauth_client_details VALUES ('public-ios-client', NULL, '{bcrypt}$2a$10$7JVJT.LxsZRJ48fpcHQrdeYytSA/8IZxgTqt3WS50AMPe.HAcBcmC', 'read,write', 'password,authorization_code,refresh_token', NULL, 'ROLE_CLIENT', 1800, 86400, NULL, NULL);
INSERT INTO public.oauth_client_details VALUES ('public-client', NULL, '{bcrypt}$2a$10$7JVJT.LxsZRJ48fpcHQrdeYytSA/8IZxgTqt3WS50AMPe.HAcBcmC', 'read,write', 'password,authorization_code,refresh_token', NULL, 'ROLE_CLIENT', 1800, 86400, NULL, NULL);

-------------------------------------------------------------------

CREATE TABLE oauth_client_token (
    token_id character varying(256),
    token bytea,
    authentication_id character varying(256) NOT NULL,
    user_name character varying(256),
    client_id character varying(256),
    constraint oauth_client_token_pkey primary key (authentication_id)
);

-------------------------------------------------------

CREATE TABLE oauth_code (
    code character varying(256),
    authentication bytea
);

------------------------------------------------------

CREATE TABLE oauth_refresh_token (
    token_id character varying(256) NOT NULL,
    token bytea,
    authentication bytea,
    constraint oauth_refresh_token_pkey primary key (token_id)
);
