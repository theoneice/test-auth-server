
-- users
CREATE INDEX users_city_idx ON public.users (city_id);

-- group_authorities
CREATE INDEX grp_auth_groups_idx ON public.group_authorities (group_id);

-- group_members
CREATE INDEX grp_mem_groups_idx ON public.group_members (group_id);
CREATE INDEX grp_mem_users_idx ON public.group_members (username);