package com.icecarev.testauthserver.config;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import java.util.List;

public class CustomJdbcDaoImpl extends JdbcDaoImpl {
    private static final String DEF_USERS_BY_USERNAME_QUERY = "select username,password,enabled "
            + "from users where username = ? or email = ?";

    @Override
    protected List<UserDetails> loadUsersByUsername(String username) {
        return getJdbcTemplate().query(DEF_USERS_BY_USERNAME_QUERY,
                new String[] { username, username }, (rs, rowNum) -> {
                    String username1 = rs.getString(1);
                    String password = rs.getString(2);
                    boolean enabled = rs.getBoolean(3);
                    return new User(username1, password, enabled, true, true, true,
                            AuthorityUtils.NO_AUTHORITIES);
                });
    }
}
