package com.icecarev.testauthserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestAuthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestAuthServerApplication.class, args);
	}

}
