package com.icecarev.testauthserver.domain.city;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RequiredArgsConstructor
@Transactional
@Service
public class CityFacade {

    private final CityRepository cityRepository;

    public List<CityView> fetchAll() {
        return cityRepository.findAll().stream()
                .map(City::toView)
                .collect(Collectors.toList());
    }
}
