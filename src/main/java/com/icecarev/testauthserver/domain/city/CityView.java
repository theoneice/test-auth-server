package com.icecarev.testauthserver.domain.city;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class CityView implements Serializable {

    public Long id;
    public String name;
    public String code;
}
