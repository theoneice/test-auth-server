package com.icecarev.testauthserver.domain.city;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/cities")
class CityResource {

    private final CityFacade cityFacade;

    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CityView> fetchAll() {
        return cityFacade.fetchAll();
    }
}
