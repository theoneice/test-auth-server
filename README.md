# User guide

### Pre startup
Before starting the auth server, you need to generate a new public ssh key and configure it in the application

* Open a terminal and run the command "ssh-genkey -o"
* You will be asked where to save the new key pair (public and private keys), select an appropriate location
* If you are asked for a password, don't enter any
* After the keys are generated, copy the location of the public key (usually id_rsa.pub) and configure it in 
  the property ssh.public-key as e.g. "file:C:/Users/TAACAIC1/id_rsa.pub". Remember to set a "file:" in front of 
  the location. This indicates to spring that this is an external file, NOT something in the project.
* When you are done with this, your authentication and authorization should be working with an OAuth2 flow